
// (1) Counting the Total # of Fruits for Sale
db.fruits.aggregate([
    {$match: {onSale: true}}, 
    {$count: "fruitsOnSale"},
    {$out: "numFruitsForSale"}
])

// (2) Counting the Total # of Fruits with Stock >= 20
db.fruits.aggregate([
    {$match: {stock: {$gte: 20}}}, 
    {$count: "enoughStock"},
    {$out: "numFruitsWithEnoughStocks"}
])

// (3) Getting the Average Price of Fruits onSale per Supplier
db.fruits.aggregate([
    {$match: {onSale: true}}, 
    {$group: {_id: "$supplier_id", avg_price: {$avg: "$price"}}},
    {$out: "avgPriceFruitsOnSalePerSupplier"}
])

// (4) Getting the Highest Price of a Fruit per Supplier
db.fruits.aggregate([
    {$match: {onSale: true}}, 
    {$group: {_id: "$supplier_id", max_price: {$max: "$price"}}},
    {$out: "highestFruitPricePerSupplier"}
])

// (5) Getting the Lowest Price of a Fruit per Supplier
db.fruits.aggregate([
    {$match: {onSale: true}}, 
    {$group: {_id: "$supplier_id", min_price: {$min: "$price"}}},
    {$out: "lowestFruitPricePerSupplier"}
])